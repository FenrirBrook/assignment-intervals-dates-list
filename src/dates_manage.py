from datetime import datetime, timedelta


def convert_to_date(date):
    return datetime.strptime(date, "%m/%d/%Y")


def create_list_of_dates(start, interval_days):
    return [str((start + timedelta(days = date)).strftime("%m/%d/%Y").lstrip('0')) for date in range(interval_days + 1)]


def is_date(input):
    try:
        return bool(convert_to_date(input))
    except:
        return False

#main func job 1
def get_dates_in_interval(start_date, end_date):

    if not is_date(start_date) or not is_date(end_date):
        return None
        
    start = convert_to_date(start_date)
    interval_days = (convert_to_date(end_date) - start).days
    if interval_days < 0:
        return None

    dates_list = []
    dates_list = create_list_of_dates(start, interval_days)
    return dates_list

def create_list_of_dict_dates(default_value, dates_list):
     return [{'date': date, 'participants': default_value} for date in dates_list]

#main func job 2
def get_default_date_data(start_date, end_date, default_value):
    dates_list = get_dates_in_interval(start_date, end_date)
    if dates_list is None:
        return []  
    return create_list_of_dict_dates(default_value, dates_list)