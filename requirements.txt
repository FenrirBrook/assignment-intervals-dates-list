coverage==4.5.2
pytest==7.1.3
pytest-cov==3.0.9
pytest-flakes==2.0.0
pytest-pep8==1.0.6
pytest-pycodestyle
pytest-pythonpath
docker