This activity has to be developed as follows:
*A method that, given a start date and an end date, generates a list of dates in that range
*A method that, given a start date, an end date and default value generates a list of dates in that range
    you need to return a list where each element will be a dictionary as follows
    [{'date': ''9/12/2022'', 'participants': -99999},{'date': ''9/13/2022'', 'participants': -99999}]